import React from 'react'
import { connect } from 'react-redux'
import coin from '../assets/images/icons/coin.svg'
import actionUser from '../actions/actionUser'
import fetchUser from '../actions/actionUser'
import Alert from './Alert'

class Product extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			product: '',
			inCart: '',
		}
	}
	redeem(product, updateUser){
		// alert(product._id)
		this.setState({inCart: true})
		const data = {"productId": product._id};
		const url = 'https://private-anon-b03faaba5c-aerolabchallenge.apiary-proxy.com/redeem';
		const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTY5NWYwNTlkZjgxMTAwNmM3MmJmYTIiLCJpYXQiOjE1ODM5NjM5MDl9.VBo0BFBLS6OXbjiRDZUDujW5zSQdcSsccvDJY_9c6Dg'

		fetch(url, {
			method: 'POST',
			body: JSON.stringify(data),
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`,
			}
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => updateUser)
		setTimeout(()=>{
			this.setState({inCart: false})
		}, 4000)
	}
	render(){
		return (
			<div className="product-item">
				<Alert isActive={this.state.inCart} message="Su producto fue agregado"></Alert>
				<div className="bag prt"></div>
				<picture>
					<img src={this.props.product.img.url} srcset={`${this.props.product.img.hdUrl} 2x`} alt={this.props.product.name} />
				</picture>
				<div className="product-data">
					<h5>{this.props.product.category}</h5>
					<h3>{this.props.product.name}</h3>
				</div>
				<div className="overlay">
					<div className="product-price">
							{this.props.product.cost} 
							<img src={coin} width="26" />
					</div>
					{/* {console.log(this.props.store)} */}
					<button className="btn btn-white" onClick={()=>this.redeem(this.props.product, this.props.updateUser())}>
						{this.state.inCart ? 'Added' : 'Redeem now'}
					</button>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	store: state
})
const mapActionsToProps = dispatch => ({
	updateUser(){
		const url = 'https://coding-challenge-api.aerolab.co/user/me';
		const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTY5NWYwNTlkZjgxMTAwNmM3MmJmYTIiLCJpYXQiOjE1ODM5NjM5MDl9.VBo0BFBLS6OXbjiRDZUDujW5zSQdcSsccvDJY_9c6Dg'

		fetch(url, {
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`,
			}
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => dispatch({ type: 'FETCH_USER', payload: response }));
	}
})

export default connect(mapStateToProps, mapActionsToProps)(Product);
