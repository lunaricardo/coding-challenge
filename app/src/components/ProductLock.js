import React from 'react';
import coin from '../assets/images/icons/coin.svg';
import { connect } from 'react-redux';

const ProductLock = (props, {removeFromCart}) => {
	const left = props.product.cost - props.store.reducerUser.user.points
	return (
		<div className="product-item not-available">
			<button className="btn btn-dark prt">
				<span className="user-money-value">You need {left}</span>
				<img src={coin} width="26" />
			</button>
			<picture>
			<img src={props.product.img.url} srcset={`${props.product.img.hdUrl} 2x`} alt={props.product.name} />
			</picture>
			<div className="product-data">
				<h5>{props.product.category}</h5>
				<h3>{props.product.name}</h3>
			</div>
			<div className="overlay">
				<div className="product-price">
					You need <br/> {left}
					<img src={coin} width="26" />
				</div>
			</div>
		</div>
	)
}

const mapStateToProps = state => ({
	store: state
})
const mapActionsToProps = dispatch => ({
	removeFromCart(product){
		dispatch({
			type: "REMOVE_TO_CART",
			product
		})
	}
})

export default connect(mapStateToProps, mapActionsToProps)(ProductLock);
