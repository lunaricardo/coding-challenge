import React from 'react';
import { connect } from 'react-redux';
import logo from '../assets/images/aerolab-logo.svg';
import coin from '../assets/images/icons/coin.svg';
import actionUser from '../actions/actionUser';


const HeaderNav = ({store}) => {
	return (
		<header>
			<div className="container">
				<nav className="navbar">
						<div className="brand">
							<img src={logo} alt="Aerolab" />
						</div>
						<div className="user">
							<div className="user-name">{store.reducerUser.user.name}</div>
							<button className="user-money btn btn-default">
								<span className="user-money-value">{store.reducerUser.user.points}</span>
								<img src={coin} width="24" alt="coin"/>
							</button>
						</div>
				</nav>
			</div>
		</header>
	)
}

const mapStateToProps = state => ({
	store: state
})

export default connect(mapStateToProps, actionUser)(HeaderNav);