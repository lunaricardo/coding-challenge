import React from 'react';
import { connect } from 'react-redux';
import Product from './Product';
import ProductLock from './ProductLock';
import image1 from '../assets/images/header-x1.png';
import image2 from '../assets/images/header-x2.png';

class Products extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			products: [],
			isFetching: false,
		}
	}

	componentDidMount(){
		const url = 'https://coding-challenge-api.aerolab.co/products';
		const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTY5NWYwNTlkZjgxMTAwNmM3MmJmYTIiLCJpYXQiOjE1ODM5NjM5MDl9.VBo0BFBLS6OXbjiRDZUDujW5zSQdcSsccvDJY_9c6Dg'

		fetch(url, {
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`,
			}
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => this.setState({products: response, isFetching: false}));
	}

	render(){
		return (
			<main>
				<div className="category-header">
						<h1>Electronics</h1>
					
					<img src={image1} srcset={`${image2} 2x`} />
				</div>
				<div className="category-body">
					<div className="container">
							<div className="category-body-row top">
								<div className="counter-page">16 of 32 products</div>
								<div className="cont-sort-desktop">
									<div className="sortby">Sort by:</div>
									<ul>
										<li><button className="btn btn-default">Most Recent</button></li>
										<li><button className="btn btn-default">Lowest price</button></li>
										<li><button className="btn btn-default">Highest price</button></li>
									</ul>
								</div>
								<div className="cont-sort-mobile">
									<div className="sortby">Sort by:</div>
									<select>
										<option>Most Recent</option>
										<option>Lowest price</option>
										<option>Highest price</option>
									</select>
								</div>
								{/* <div className="cont-arrows">
									<button><img src="assets/images/icons/arrow-left.svg" alt=""></button>
									<button><img src="assets/images/icons/arrow-right.svg" alt=""></button>
								</div> */}
							</div>
							<div className="category-products">
							{
								// console.log(this.props.store.reducerUser.user.points)
								this.state.products.map(product => (
									product.cost > this.props.store.reducerUser.user.points ?
									<ProductLock product={product} key={product.id} />
									:
									<Product product={product} key={product.id} />
									)
								)
							}
							</div>
							<div className="category-body-row bottom">
								<div className="counter-page">16 of 32 products</div>
								{/* <div className="cont-arrows">
									<button><img src="assets/images/icons/arrow-left.svg" alt=""></button>
									<button><img src="assets/images/icons/arrow-right.svg" alt=""></button>
								</div> */}
							</div>
					</div>
					</div>
			</main>
		) 
	}
}
const mapStateToProps = state => ({
	store: state
})
export default connect(mapStateToProps,)(Products);