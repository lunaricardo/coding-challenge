import React, { useState } from 'react'

const Alert = (props) => {
    const [state, setState] = useState(false)
    if(!props.isActive){
        return null
    }
    return (
        <div className="alert alert-success">{props.message}</div>
    )
}

export default Alert