const initialState = {
	user:{},
	cart:[]
}

const ReducerUser = (state = initialState, actions) => {
	// console.log(actions)
	switch (actions.type) {
		case 'FETCH_USER':
			return {
				...state,
				user: actions.payload
			}
			break;
		default:
			break;
	}
	return state
}

export default ReducerUser