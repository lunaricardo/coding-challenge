const FETCH_USER = 'FETCH_USER'
const fetchUser = (dispatch) => {
	return (dispatch) => {
		console.log("despacha")
		const url = 'https://coding-challenge-api.aerolab.co/user/me';
		const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTY5NWYwNTlkZjgxMTAwNmM3MmJmYTIiLCJpYXQiOjE1ODM5NjM5MDl9.VBo0BFBLS6OXbjiRDZUDujW5zSQdcSsccvDJY_9c6Dg'

		fetch(url, {
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`,
			}
		}).then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => dispatch({ type: FETCH_USER, payload: response }));
	}
	//dispatch({ type: FETCH_USER, data })
}

export default fetchUser