import React, { Fragment } from 'react'
import {Provider} from 'react-redux'
import HeaderNav from './components/HeaderNav'
import Products from './components/Products'
import './assets/css/styles.css'
import store from './store'


const App = () => (
  <Provider store={store}>
    <Fragment>
      <HeaderNav />
      <Products />
    </Fragment>
  </Provider>
)

export default App;
